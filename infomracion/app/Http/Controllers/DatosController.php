<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dato;

class DatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos = Dato::all();
        return view('datos.index',['dato'=>$datos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('datos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dato = new Dato();
        $dato->nombre = $request->nombre;
        $dato->apellidop = $request->apellidop;
        $dato->apellidom = $request->apellidom;
        $dato->fecha = $request->fecha;

        if ($dato->save()){
            return redirect('/datos');
        }else{
            return view('datos.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $dato = Dato::find($id);
        return view('datos.edit',['dato'=>$dato]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dato = Dato::find($id);
        $dato->nombre = $request->nombre;
        $dato->apellidop = $request->apellidop;
        $dato->apellidom = $request->apellidom;
        $dato->fecha = $request->fecha;

        if ($dato->save()){
            return redirect('datos');
        }else{
            return view('datos.edit',['dato'=>$dato]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Dato::destroy($id);
        return redirect('/datos');
    }
}