@extends('layouts.app')
@section('content')
 <div class="">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Registros</h1>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO PATERNO</th>
                        <th>APELLIDO MATERNO</th>
                        <th>FECHA NACIMIENTO</th>
                        <th style="text-align: center">ACCION</th>
                    </tr>
                    </thead>
                    @foreach($dato as $d)
                        <tr>
                            <td align="center">{{$d->id}}</td>
                            <td align="center">{{$d->nombre}}</td>
                            <td align="center">{{$d->apellidop}}</td>
                            <td align="center">{{$d->apellidom}}</td>
                            <td align="center">{{$d->fecha}}</td>
                            <td style="background-color: lightblue">
                                <a href="{{url('/datos/'.$d->id.'/edit')}}" class="btn btn-link">Editar</a>
                                
                            </td>
                        </tr>
                    @endforeach
                </table>
                <form action="{{route('datos.create')}}" method="get">
                    <input class="btn btn-success" type="submit" value="Nuevo">
                </form>
            </div>
        </div>
    </div>
@endsection