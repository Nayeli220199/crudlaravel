@extends('layouts.app')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 style="text-align: center">Datos Personales</h1>
                <form action="{{route('datos.store')}}" method="post">
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input name="nombre" type="text" class="form-control" id="nombre">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Apellido Paterno</label>
                        <input name="apellidop" type="text" class="form-control" id="apellidop">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Apellido Materno</label>
                        <input name="apellidom" type="text" class="form-control" id="apellidom">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Fecha de Nacimiento</label>
                        <input name="fecha" type="date" class="form-control" id="fecha">
                    </div>
                    <input type="submit" class="btn btn-primary" value="Guardar">
                </form>
            </div>
        </div>
    </div>
@endsection